@AmazonChart

Feature: User Chart Actions

    Scenario Outline: As a shopper, I want to make changes at chart
        Given Amazon website is open
        When User search "<item>" item
        Then User access to first item
        And User adds "<initial_items>" to chart
        When User access to chart
        Then Chart has "<initial_items>" and the total amount is correct
        When User changes item quantity by "<modified_items>"
        Then Chart has "<modified_items>" and the total amount is correct

    @Case1
    Examples:
        | item          | initial_items | modified_items |
        | hats for men  | 2             | 1              |
        | hats for men  | 2             | 3              |