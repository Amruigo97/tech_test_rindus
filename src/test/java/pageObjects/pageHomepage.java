package pageObjects;

import org.openqa.selenium.By;

public class pageHomepage {
    public static By searchBar = By.id("twotabsearchtextbox");
    public static By firstElement = By.xpath("//div[@class='s-main-slot s-result-list s-search-results sg-row']/div[3]//div[span[@data-component-type='s-product-image']]");
    public static By chartBtn = By.id("nav-cart-count-container");

}
