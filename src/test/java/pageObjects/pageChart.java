package pageObjects;

import org.openqa.selenium.By;

public class pageChart {
    public static By quantityBtn = By.xpath("//span[label[@for='quantity']]//span[@data-action='a-dropdown-button']");
    public static By quantityInput = By.xpath("//input[@name='quantityBox']");
    public static By currentItemQuantity = By.xpath("//span[@class='a-dropdown-prompt']");
    public static By itemPriceBox = By.xpath("//span[@class='a-size-medium a-color-base sc-price sc-white-space-nowrap sc-product-price a-text-bold']");
    public static By currentChartAmount = By.xpath("//span[@id='sc-subtotal-amount-activecart']/span");
}
