package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.screenshotFailure;
import webdriverSupport.webDriverManager;

public class Hooks {
    @Before
    public void before (Scenario scenario) {
        System.out.println();
        System.out.println("----------------------------------------------------------------");
        System.out.println("Executing scenario: '"+scenario.getName()+"' with associated tags: "+scenario.getSourceTagNames());
        System.out.println("----------------------------------------------------------------");
    }

    @After
    public void after(Scenario scenario) {
        if (scenario.isFailed()) {
            System.out.println("Taking screenshot...");
            screenshotFailure.screenshot(webDriverManager.getDriver(), scenario.getName(), scenario.getSourceTagNames());
        }
        webDriverManager.close();
    }
}