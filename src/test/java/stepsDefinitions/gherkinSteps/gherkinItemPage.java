package stepsDefinitions.gherkinSteps;

import io.cucumber.java.en.And;

import static stepsDefinitions.javaSteps.stepsItemPage.*;

public class gherkinItemPage {

    @And("User adds {string} to chart")
    public void user_adds_to_chart(String itemNb) {
        addToChart(itemNb);
    }

    
}
