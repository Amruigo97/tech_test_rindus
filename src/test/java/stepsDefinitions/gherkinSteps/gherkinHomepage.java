package stepsDefinitions.gherkinSteps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static stepsDefinitions.javaSteps.stepsHomepage.*;

public class gherkinHomepage {

    @Given("Amazon website is open")
    public void amazon_website_is_open() {
        setupWeb();
        sendURL("https://amazon.com");
    }

    @When("User search {string} item")
    public void user_search_item(String item) {
        searchItem(item);
    }

    @Then("User access to first item")
    public void user_access_to_first_item() {
        accessFirstItem();
    }

    @When("User access to chart")
    public void user_access_to_chart() {
        userAccessToChart();
    }
}
