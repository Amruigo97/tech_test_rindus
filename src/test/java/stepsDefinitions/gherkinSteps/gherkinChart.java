package stepsDefinitions.gherkinSteps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static stepsDefinitions.javaSteps.stepsChart.*;

public class gherkinChart {
    @Then("Chart has {string} and the total amount is correct")
    public void chart_has_items_and_amount_is_correct(String itemNb) {
        chartHasItems(itemNb);
        checkTotalAmount(itemNb);
    }

    @When("User changes item quantity by {string}")
    public void user_changes_item_quantity(String itemNb) {
        changeItemQuantity(itemNb);
    }
}
