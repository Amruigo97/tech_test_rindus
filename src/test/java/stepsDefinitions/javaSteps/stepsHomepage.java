package stepsDefinitions.javaSteps;

import org.openqa.selenium.Keys;
import webdriverSupport.webDriverManager;

import java.net.MalformedURLException;

import static webdriverSupport.seleniumHelper.*;
import static pageObjects.pageHomepage.*;

public class stepsHomepage {
    public static void setupWeb() {
        try {
            webDriverManager.setDriver();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void sendURL(String URL) {
        webDriverManager.getDriver().get(URL);
    }

    public static void searchItem(String item) {
       clickElement(searchBar);
       inputText(searchBar, item);
       sendKey(searchBar, Keys.ENTER);
       waitUntilPageIsFullyLoaded();
    }

    public static void accessFirstItem() {
        clickElement(firstElement);
    }

    public static void userAccessToChart() {
        clickElement(chartBtn);
    }

}
