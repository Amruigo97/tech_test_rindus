package stepsDefinitions.javaSteps;

import org.junit.Assert;
import org.openqa.selenium.By;

import static pageObjects.pageChart.*;
import static webdriverSupport.seleniumHelper.*;

public class stepsChart {
    public static void chartHasItems(String itemNb) {
        Assert.assertEquals("Items quantity at chart does not match with requested ones", itemNb, getElementText(currentItemQuantity));
    }
    public static void checkTotalAmount(String itemNb) {
        String singleItemPrice = getElementText(itemPriceBox);
        singleItemPrice = singleItemPrice.replaceAll("[^\\d.]", "");
        String expectedChartAmount = String.valueOf((Double.parseDouble(singleItemPrice) * Integer.parseInt(itemNb)));
        Assert.assertEquals("Items quantity at chart does not match with requested ones", expectedChartAmount,
                getElementText(currentChartAmount).replaceAll("[^\\d.]", ""));
    }
    public static void changeItemQuantity(String itemQuantity) {
        if (Integer.parseInt(itemQuantity) < 10) {
            clickElement(quantityBtn);
            clickElement(By.id("quantity_"+itemQuantity));
        } else {
            clickElement(quantityBtn);
            clickElement(By.id("quantity_10"));
            inputText(quantityInput, itemQuantity);
        }
        reloadPage();
    }
}
