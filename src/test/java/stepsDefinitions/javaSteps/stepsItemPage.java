package stepsDefinitions.javaSteps;

import org.openqa.selenium.By;

import static webdriverSupport.seleniumHelper.*;
import static pageObjects.pageItemPage.*;
import static pageObjects.pageChart.quantityBtn;

public class stepsItemPage {
    public static void addToChart(String nb) {
        if(Integer.parseInt(nb)>1 & Integer.parseInt(nb)<30) {
            clickElement(quantityBtn);
            clickElement(By.id("quantity_"+(Integer.parseInt(nb)-1)));
            clickElement(addToCharBtn);
        } else if (nb.equals("1")) {
            clickElement(addToCharBtn);
        } else {
            throw new IllegalArgumentException("User only can add less than 31 units at item page.");
        }
    }
}
