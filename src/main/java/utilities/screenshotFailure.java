package utilities;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class screenshotFailure {
    public static void screenshot(WebDriver driver, String name, Collection<String> tag) {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        try{
            FileHandler.copy(source, new File("target/cucumber-report/"+name+"_"+tag+".png"));
        } catch (IOException e) {
            System.out.println("Exception while taking screenshot "+e.getMessage());
        }
    }
}
