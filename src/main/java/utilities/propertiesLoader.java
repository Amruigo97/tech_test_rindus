package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class propertiesLoader {
    static Properties props = new Properties();
    static String filePath = "src/test/resources/environment.properties";
    static File file = new File(filePath);
    static FileInputStream  fileInput = null;

    public static Properties getProperties() {
        try {
            fileInput = new FileInputStream(file);
            props.load(fileInput);
        } catch (FileNotFoundException e) {
            logger("File was not found");
        } catch (IOException e) {
            logger("IOExpection through");
        } finally {
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
                    logger("No close successfully");
                }
            }
        }
        return props;
    }

    public static void logger(String string) {
        Logger logger = Logger.getLogger("com.api.jar");
        logger.info(string);
    }
}