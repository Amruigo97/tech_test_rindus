package webdriverSupport;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverLogLevel;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import utilities.propertiesLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;

import static utilities.propertiesLoader.getProperties;

public class webDriverManager {
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private static final String browser = getProperties().getProperty("browser");
    private static final String downloadDirectory = System.getProperty("user.dir")+ File.separator+"scr"+File.separator
            +"test" +File.separator+"resources"+File.separator+"downloads";

    public static void setDriver() throws MalformedURLException {
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        //Switch instead of if-else since it allows to add more browsers easily
        switch (browser) {
            case "firefox" -> {
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.download.dir", downloadDirectory);
                FirefoxOptions f_options = new FirefoxOptions();
                f_options.setProfile(profile);
                if (propertiesLoader.getProperties().getProperty("OS").equals("linux")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/java/linux/geckodriver");
                } else {
                    System.setProperty("webdriver.gecko.driver", "src/main/java/drivers/windows/geckodriver.exe");
                }
                if (propertiesLoader.getProperties().getProperty("MAXIMIZED").equals("true")) {
                    f_options.addArguments("--start-maximized");
                }
                if (propertiesLoader.getProperties().getProperty("HEADLESS").equals("true")) {
                    f_options.addArguments("--headless");
                }
                f_options.addArguments("--remote-allow-origins=*");
                f_options.setLogLevel(FirefoxDriverLogLevel.FATAL);
                driver.set(new FirefoxDriver(f_options));
            }
            default -> {
                Map<String, Object> chromePrefs = new HashMap<>();
                chromePrefs.put("download.default_directory", downloadDirectory);
                ChromeOptions c_options = new ChromeOptions();
                c_options.setExperimentalOption("prefs", chromePrefs);
                if (propertiesLoader.getProperties().getProperty("OS").equals("linux")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/java/linux/chromedriver");
                } else {
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/windows/chromedriver.exe");
                }
                if (propertiesLoader.getProperties().getProperty("MAXIMIZED").equals("true")) {
                    c_options.addArguments("--start-maximized");
                }
                if (propertiesLoader.getProperties().getProperty("HEADLESS").equals("true")) {
                    c_options.addArguments("--headless");
                }
                c_options.addArguments("--remote-allow-origins=*");
                c_options.setLogLevel(ChromeDriverLogLevel.OFF);
                driver.set(new ChromeDriver(c_options));
            }
        }
    }

    public static WebDriver getDriver() {return Objects.requireNonNull(driver.get());}

    public static void close() {
        if (propertiesLoader.getProperties().getProperty("keep.browser.open").equals("false")) {
            getDriver().close();
            getDriver().quit();
            System.out.println("Driver closed.");
        }
    }
}
