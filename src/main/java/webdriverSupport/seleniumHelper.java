package webdriverSupport;

import com.jcabi.aspects.RetryOnFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class seleniumHelper {
    private static final WebDriverWait wait = new WebDriverWait(webDriverManager.getDriver(), Duration.ofSeconds(10));
    @RetryOnFailure(attempts = 10, delay = 1, verbose = false)
    public static void clickElement (By element) {
        webDriverManager.getDriver().findElement(element).click();
    }

    @RetryOnFailure(attempts = 10, delay = 1, verbose = false)
    public static void inputText(By element, String text) {
        //webDriverManager.getDriver().findElement(element).clear();
        webDriverManager.getDriver().findElement(element).sendKeys(text);
    }

    public static void sendKey(By element, Keys key) {
        webDriverManager.getDriver().findElement(element).sendKeys(key);
    }

    public static String getElementText(By element) {
        return webDriverManager.getDriver().findElement(element).getText();
    }

    public static void waitUntilPageIsFullyLoaded() {
        wait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public static void reloadPage() {
        webDriverManager.getDriver().navigate().refresh();
    }
}
