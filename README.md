# INSTALLATION STEPS
Install Maven:
    - Download Maven binary
    - Set MAVEN_HOME at your environment variables
    - Add binary folder to path
    - Further installation guide: https://phoenixnap.com/kb/install-maven-windows

Install JDK:
    - Downlad JDK (Oracle or OpenJDK)
    - Set JAVA_HOME at your environment variables

# PROJECT STRUCTURE
This project is mainly distributed in two main files, 'main' and 'test'.
## What you can find in 'main'
In 'main' are the main configuration files, as well as libraries and utilities that are common to all types of tests.
 - drivers: at drivers we can find the webdrivers executables to run the web tests. If your browser has different version than development one, download another one and locate it there. You can download drivers from:
   - Chrome: https://chromedriver.chromium.org
   - Firefox: https://github.com/mozilla/geckodriver/releases
 - utilities: here are located all java files to support automation which are not directly involved on it.
 - webdriverSupport: setup and teardown webdriver and libraries to support automation directly.
## What you can find in 'test'
In 'test' are the use case automation files, from the tests themselves to the step execution files.
 - java.hooks: at hooks we can find only one file, Hooks, which is the test pre-processor and post-processor.
 - java.runner: cukeRunner to run test, further explanation at next section.
 - java.pageObjects: java files where page locators are stored to be shared between test steps.
 - java.stepsDefinitions: here we can find two folders. GherkinSteps is implementing all test steps used at feature file. JavaSteps is implementing all java code involved in execute steps.
 - resources.features: folder in which test are located.

# HOW TO EXECUTE
You can execute tests by running the test runner (called cukeRunner) or by command line.
## Executing by cukeRunner
You can find cukeRunner at _src/test/java/runner/cukeRunner.java_
Depending on your IDE, run this file in which mode is available.
At runner, you will find:
- features = "src/test/resources/features" -> Here is the folder where tests are located
- glue = {"stepsDefinitions","hooks"} -> You need to glue whatever dependencies are needed
- tags = "@AmazonChart" -> You can add tags to execute a certain test amount (the ones tagged)

## Executing by command line
For executing commands to run test you should go to project root at CMD.

Command:
```
mvn exec:java -Dexec.classpathScope=test -Dexec.mainClass=io.cucumber.core.cli.Main -Dexec.args="src/test/resources/features --glue stepsDefinitions --tags @AmazonChart"
```
# RUN OPTIONS
At _src/test/resources/environment.properties_ you can find executions options such as:
 - MAXIMIZED = true or false -> maximize the execution window
 - OS = windows or linux -> for choosing webdriver
 - keep.browser.open = true or false -> if true, browser will not be closed after execution
 - browser = chrome or firefox -> choose web browser
# REPORTS
Reports are located in 'target' folder, which is auto-generated.

Here, you can find a folder called _cucumber-report_, in which you find the html report generated.

Screenshots at failure are available at this project and stored at _cucumber-report_ too.